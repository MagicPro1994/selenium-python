# Selenium Python Framework

* What can this framework do:
  * Easy to maintain
  * Run test-cases parallel with pytest

## Requirements

* Python >= 3.7.3
* Selenium >= 3.140
* Optional:
  * VSCode lastest version

## Selenium Downloads

![Selenium Homepage](https://www.seleniumhq.org/download/)

![Geckodriver](https://github.com/mozilla/geckodriver/releases)

![ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads)
