# For Chome Configuration
- executable_path - path to the executable. If the default is used it assumes the executable is in the $PATH
- port - port you would like the service to run, if left as 0, a free port will be found.
- options - this takes an instance of ChromeOptions
- service_args - List of args to pass to the driver service
- desired_capabilities - Dictionary object with non-browser specific capabilities only, such as "proxy" or "loggingPref".
- service_log_path - Where to log information from the driver.
- keep_alive - Whether to configure ChromeRemoteConnection to use HTTP keep-alive.

# For Firefox Configuration
- firefox_profile: Instance of ``FirefoxProfile`` object or a string.  If undefined, a fresh profile will be created in a temporary location on the system.
- firefox_binary: Instance of ``FirefoxBinary`` or full path to the Firefox binary.  If undefined, the system default Firefox installation will  be used.
- timeout: Time to wait for Firefox to launch when using the extension connection.
- capabilities: Dictionary of desired capabilities.
- proxy: The proxy settings to us when communicating with Firefox via the extension connection.
- executable_path: Full path to override which geckodriver binary to use for Firefox 47.0.1 and greater, which defaults to picking up the binary from the system path.
- options: Instance of ``options.Options``.
- service_log_path: Where to log information from the driver.
- service_args: List of args to pass to the driver service
- desired_capabilities: alias of capabilities. In future versions of this library, this will replace 'capabilities'. This will make the signature consistent with RemoteWebDriver.
- keep_alive: Whether to configure remote_connection.RemoteConnection to use HTTP keep-alive.