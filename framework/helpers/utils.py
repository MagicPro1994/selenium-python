import os
import threading
from pathlib import Path

from framework.helpers.enum_data import Browser


class Utils:
    # == General support functions ==
    @staticmethod
    def create_path(pathStr, isFile=True):
        path = Path(pathStr)
        if isFile:
            path.parent.mkdir(parents=True, exist_ok=True)
        else:
            path.mkdir(parents=True, exist_ok=True)

    # == Pytest support functions ==
    @staticmethod
    def get_testcase_driver_key(testClass, request=None, browserId=0, index=0):
        jointers = [
            os.getenv("PYTEST_XDIST_WORKER") or "main",
            Browser.get_name(browserId) or "UNKNOWN",
            type(testClass).__name__,
            request.node.name,
            str(index)
        ]
        return "-".join(jointers)

    @staticmethod
    def get_default_driver_key():
        jointers = [
            os.getenv("PYTEST_XDIST_WORKER") or "main",
            "default-driver"
        ]
        return "-".join(jointers)

    @staticmethod
    def thread_to_dict_key(threadObj=None):
        if threadObj is None:
            threadObj = threading.current_thread()
        return "-".join(threadObj.name, threadObj.ident)

    @staticmethod
    def get_worker_config(workerId="main"):
        if workerId is None:
            workerId = "main"
        configFile = "{}/{}.ini".format(os.getenv("XDIST_WORKER_CONFIG"), workerId)
        context = {
            "SELENIUM_PATH": os.getenv("SELENIUM_PATH")
        }
        Utils.execute_pyscript(configFile, context)
        return context

    @staticmethod
    def execute_pyscript(filePath, contextDict):
        try:
            with open(filePath, mode='r') as configFile:
                pyScript = configFile.read()
        except:
            # Cannot read file
            return None
        exec(pyScript, contextDict)
        print(contextDict)