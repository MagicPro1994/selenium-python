from enum import IntEnum

class Browser(IntEnum):
    # Local browser id from 1 to 10
    CHROME = 1
    FF = 2
    IE = 3
    # Remote driver id starts from 11
    REMOTE_CHROME = 11
    REMOTE_FF = 12
    REMOTE_IE = 13

    @staticmethod
    def get_name(browserId):
        try:
            return Browser(browserId).name
        except:
            return None
