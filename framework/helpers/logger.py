import logging
from .utils import create_path 

def setup_logger(loggerName, logFile, level=logging.DEBUG):
    l = logging.getLogger(loggerName)
    formatter = logging.Formatter(
        '%(asctime)s - %(threadName)s - %(thread)d - %(name)s - %(levelname)s - %(message)s',
        "%Y-%m-%d %H:%M:%S")
    create_path(logFile)
    fileHandler = logging.FileHandler(logFile, mode='a')
    fileHandler.setFormatter(formatter)
    
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)
    return l
