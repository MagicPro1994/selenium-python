import threading

from selenium import webdriver

from framework.helpers.enum_data import Browser
from framework.helpers.utils import Utils

# if '__main__' == __name__:
#     import os
#     import sys

#     from dotenv import load_dotenv

#     load_dotenv(override=True)
#     sys.path.append(os.getenv("BASEPATH"))


class BrowserManager:
    class __BrowserManagerInstance:
        def __init__(self):
            self.driverList = {}

        def set_driver(self, driver, driverKey):
            threadDriverDict = self.driverList.get(Utils.thread_to_dict_key())
            if threadDriverDict is None:
                self.driverList[Utils.thread_to_dict_key()] = {
                    # List of driver dict keys
                    "drivers": [driverKey],
                    driverKey: driver
                }
            else:
                threadDriverDict.get("drivers").append(driverKey)
                threadDriverDict.update({driverKey: driver})

        def get_driver(self, driverKey=None):
            threadDriverDict = self.driverList.get(Utils.thread_to_dict_key())
            if threadDriverDict is None:
                return None
            if driverKey is None:
                driverKey = threadDriverDict.get("drivers")[0]
            return threadDriverDict.get(driverKey)

        def terminate_all_drivers(self, currentThreadOnly=False):
            def terminate_thread_drivers(threadDictKey, threadDriverDict):
                for driverDictKey in threadDriverDict.get("drivers"):
                    try:
                        driverList.get(driverDictKey).quit()
                    except:
                        pass
                self.driverList.pop(threadDictKey)

            if len(self.driverList) == 0:
                return False

            if currentThreadOnly is True:
                _threadDict = self.driverList.get(Utils.thread_to_dict_key())
                if _threadDict is None:
                    return False
                threadDriverDict = {_threadDict}
            else:
                threadDriverDict = self.driverList

            for threadKey, threadDrivers in threadDriverDict.items():
                terminate_thread_drivers(threadKey, threadDrivers)

            return True

        def __str__(self):
            return repr(self) + repr(self.driverList)

    instance = __BrowserManagerInstance()

    def __new__(cls):  # __new__ always a classmethod
        if not BrowserManager.instance:
            BrowserManager.instance = __BrowserManagerInstance()
        return BrowserManager.instance

    @staticmethod
    def get_driver_list():
        return BrowserManager.instance.driverList

    @staticmethod
    def get_driver(browerKey=None):
        return BrowserManager.instance.get_driver(browerKey)

    @staticmethod
    def create_new_driver(driverId, driverConfig={}, driverKey="default-browser"):
        def get_selenium_driver():
            if Browser.CHROME == driverId:
                return webdriver.Chrome(**driverConfig)
            if Browser.FF == driverId:
                return webdriver.Firefox(**driverConfig)
            if Browser.IE == driverId:
                return webdriver.Ie(**driverConfig)
            # Remote browsers
            if driverId > 10:
                return webdriver.Remote(**driverConfig)
            raise Exception(
                "There is no support for driverId: {}".format(driverId))

        BrowserManager.instance.set_driver(get_selenium_driver(), driverKey)
        return BrowserManager.get_driver()

    @staticmethod
    def shutdown(currentThreadOnly=False):
        BrowserManager.instance.terminate_all_drivers(currentThreadOnly)
