import pytest
import os
import sys

from dotenv import load_dotenv

load_dotenv(override=True)
sys.path.append(os.getenv("BASEPATH"))
from framework.helpers.utils import Utils


def pytest_configure(config):
    # Load worker config
    config.slaveinput["xdist-config"] = Utils.get_worker_config(
        os.getenv("PYTEST_XDIST_WORKER"))
    # Initial default driver

@pytest.fixture(scope="session")
def setup(request):
    print(Utils.generate_driver_key(request, 2, request, 0))
