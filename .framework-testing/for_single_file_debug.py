if '__main__' == __name__:
    import os
    import sys

    from dotenv import load_dotenv

    load_dotenv(override=True)
    sys.path.append(os.getenv("BASEPATH"))

