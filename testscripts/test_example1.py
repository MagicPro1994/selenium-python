import pytest

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from framework.enhancements.browser_manager import WebDriverManager


class TestExampleOne:
    def test_title(self, get_webdriver):
        assert "Selenium Easy" in get_webdriver.title

    def test_content_text(self, get_webdriver):
        print("Verify content on the page")
        centerText = get_webdriver.find_element_by_css_selector(
            '.tab-content .text-center').text
        assert "WELCOME TO SELENIUM EASY DEMO" == centerText

    def test_bootstrap_bar(self, get_webdriver):
        print("Lets try with another example")
        mainMenu = get_webdriver.find_element_by_xpath(
            "//li/a[contains(text(), 'Progress Bars')]")
        mainMenu.click()

        subMenu = get_webdriver.find_element_by_xpath(
            "//li/a[contains(text(), 'Bootstrap Progress bar')]")
        subMenu.click()

        btnDownload = get_webdriver.find_element_by_id("cricle-btn")
        btnDownload.click()

        WebDriverWait(get_webdriver, 50).until(
            EC.text_to_be_present_in_element_value((By.ID, 'cricleinput'), "105"))

        elemValue = get_webdriver.find_element_by_id("cricleinput")
        elemVAttributealue = elemValue.get_attribute('value')
        assert elemVAttributealue == "105"

    def test_one(self):
        x = "this"
        assert 'h' in x

    def test_two(self):
        x = "hello"
        assert hasattr(x, 'check')

    def test_three(self):
        x = "welcome"
        assert hasattr(x, 'test')
