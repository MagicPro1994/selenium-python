import time

import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from framework.enhanments.browser_manager import WebDriverManager


class TestExampleTwo:
    def test_InputForm(self, get_webdriver):

        print("Another example")
        mainMenu = get_webdriver.find_element_by_xpath(
            "//li/a[contains(text(), 'Input Forms')]")
        mainMenu.click()

        subMenu = get_webdriver.find_element_by_xpath(
            "//li/a[contains(text(), 'Simple Form Demo')]")
        subMenu.click()

        # Finding "Single input form" input text field by id. And sending keys(entering data) in it.
        eleUserMessage = get_webdriver.find_element_by_id("user-message")
        eleUserMessage.clear()
        eleUserMessage.send_keys("Test Python")

        # Finding "Show Your Message" button element by css selector using both id and class name. And clicking it.
        eleShowMsgBtn = get_webdriver.find_element_by_css_selector(
            '#get-input > .btn')
        eleShowMsgBtn.click()

        # Checking whether the input text and output text are same using assertion.
        WebDriverWait(get_webdriver, 10).until(
            EC.presence_of_element_located((By.ID, 'display')))
        eleYourMsg = get_webdriver.find_element_by_id("display")
        assert "Test Python" in eleYourMsg.text

    def test_funcfast(self):
        time.sleep(0.1)

    def test_funcslow1(self):
        time.sleep(0.2)

    def test_funcslow2(self):
        time.sleep(0.3)
