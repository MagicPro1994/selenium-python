import os
import json

import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from framework.enhancements.browser_manager import BrowserManager
from framework.helpers.enum import Browser


class TestSampleTemplate:
    def setup_method(self, test_method):
        config = {}
        # if str(worker_id) == "gw0" or str(worker_id) == "gw1":
        with open('{}/{}.json'.format(os.getenv("XDIST_WORKER_CONFIG"), "gw0")) as f:
            config = json.load(f)
            chromeConfig = config["browsers"]["chrome-remote"]

        BrowserManager.create_new_driver(Browser.REMOTE_CHROME, chromeConfig)

    def teardown_method(self):
        BrowserManager.get_driver().close()

    def test_abc(self):
        driver = BrowserManager.get_driver()
        driver.get("https://anchua.net")
        assert False

    def test_abcd(self):
        driver = BrowserManager.get_driver()
        driver.get("https://www.google.com")
        assert False
